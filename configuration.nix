# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:
/*
   let home-manager = builtins.fetchTarball "https://github.com/nix-community/home-manager/archive/master.tar.gz";
   in*/ {
     imports =
       [ # Include the results of the hardware scan.
       ./hardware-configuration.nix
#  (import "${home-manager}/nixos")
       ];
# Bootloader.
     boot.loader.systemd-boot.enable = true;
     boot.loader.efi.canTouchEfiVariables = true;

     networking.hostName = "nixos"; # Define your hostname.
# networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

# Configure network proxy if necessary
# networking.proxy.default = "http://user:password@proxy:port/";
# networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

# Enable networking
       networking.networkmanager.enable = true;

# Set your time zone.
     time.timeZone = "America/Sao_Paulo";

# Select internationalisation properties.
     i18n.defaultLocale = "en_US.UTF-8";

     i18n.extraLocaleSettings = {
       LC_ADDRESS = "pt_BR.UTF-8";
       LC_IDENTIFICATION = "pt_BR.UTF-8";
       LC_MEASUREMENT = "pt_BR.UTF-8";
       LC_MONETARY = "pt_BR.UTF-8";
       LC_NAME = "pt_BR.UTF-8";
       LC_NUMERIC = "pt_BR.UTF-8";
       LC_PAPER = "pt_BR.UTF-8";
       LC_TELEPHONE = "pt_BR.UTF-8";
       LC_TIME = "pt_BR.UTF-8";
     };

# XDG Portals
     xdg = {
       autostart.enable = true;
       portal = {
	 enable = true;
	 extraPortals = [
	   pkgs.xdg-desktop-portal
	     pkgs.xdg-desktop-portal-gtk
	 ];
       };
     };

     services={
       greetd = {
	 enable = true;
	 settings = rec {
	   initial_session = {
	     command = "Hyprland";
	     user = "bigpog";
	   };
	   default_session = initial_session;
	 };
       };
# Enable CUPS to print documents.
       printing.enable = true;
       flatpak.enable = true;
     };
# Configure console keymap
     console.keyMap = "br-abnt2";


# Enable sound with pipewire.
     #sound.enable = true;
     hardware.pulseaudio.enable = false;
     security.rtkit.enable = true;
     services.pipewire = {
       enable = true;
       alsa.enable = true;
       alsa.support32Bit = true;
       pulse.enable = true;
     };
#Hyprland + waybar enabling
     programs={
       hyprland={
	 enable=true;
	 xwayland.enable=true;
       };
       waybar.enable=true; 
       neovim={
	 enable=true;
	 defaultEditor=true;

       };
     };
     fonts.packages=with pkgs;[
       nerdfonts
     ];

# Define a user account. Don't forget to set a password with ‘passwd’.
     users.users.bigpog = {
       isNormalUser = true;
       description = "bigpog";
       extraGroups = [ "networkmanager" "wheel" ];
       packages = with pkgs; [
	 librewolf
	   lyrebird
	   musikcube
       ];
     };

# Allow unfree packages
     nixpkgs.config.allowUnfree = true;

     environment={
       systemPackages= with pkgs;[
# C moon development stuff
	 sqlitebrowser
	   gnumake
	   pkg-config
	   gcc
	   cargo 
	   steam-run
	   git
	   wget
	   vulkan-headers
	   vulkan-validation-layers
	   vulkan-tools
	   vulkan-loader
#General desktop utilities
	   mc
	   flatpak
	   neofetch
	   alacritty
	   cliphist
hyprpaper
	   swww
	   grimblast
	   nerdfonts
	   rofi 
	   unzip
	   ];
     };
# List services that you want to enable:

# Enable the OpenSSH daemon.
# services.openssh.enable = true;

# Open ports in the firewall.
# networking.firewall.allowedTCPPorts = [ ... ];
# networking.firewall.allowedUDPPorts = [ ... ];
# Or disable the firewall altogether.
# networking.firewall.enable = false;

# This value determines the NixOS release from which the default
# settings for stateful data, like file locations and database versions
# on your system were taken. It‘s perfectly fine and recommended to leave
# this value at the release version of the first install of this system.
# Before changing this value read the documentation for this option
# (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
     system.stateVersion = "23.11"; # Did you read the comment?

   }
